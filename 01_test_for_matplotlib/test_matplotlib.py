# one simple bar char is showing
import matplotlib.pyplot as plt

# Axis
x = [1, 2,  3, 4, 5]
y = [100, 200, 300, 400, 500]

# making label and showing the b ar

plt.barh(x, y, color='blue', label='The Label for Legend')
plt.title('The Title')
plt.xlabel('X Label')
plt.ylabel('Y Label')
plt.legend(facecolor='gray', shadow=True, loc=5, title='title for legend')
plt.show()
